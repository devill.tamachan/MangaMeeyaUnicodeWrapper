[![Build status](https://ci.appveyor.com/api/projects/status/ao05qgtnareeiwuf?svg=true)](https://ci.appveyor.com/project/tamachan/mangameeyaunicodewrapper-unp0r)

# MangaMeeyaUnicodeWrapper

マンガミーヤでUnicode文字を使ったファイルを読めるようにするソフト

### ダウンロード
 - https://gitlab.com/devill.tamachan/MangaMeeyaUnicodeWrapper/releases

### インストール方法

 - MangaMeeya.exeのフォルダへ３つのdllをコピー貼り付けする（_ernel32.dll, _hell32.dll, _omdlg32.dll）
 - CFF Explorerをダウンロードする
 - CFF ExplorerでMangaMeeya.exeを開く
 - HowToInstall.jpgを見ながら書き換える（Kernel32.dll -> _ernel32.dll,  Shell32.dll -> _hell32.dll,  comdlg32.dll -> _omdlg32.dll）
 - Saveする（上書きするかどうか聞かれるのでYes）
 - 終わり

![HowToInstall](HowToInstall.jpg)


### 対応バージョン

 - MangaMeeya_73.zipのみで動作確認
 - 他のバージョンで動くかは未確認


### 仕組み
 - ファイルパスを短いファイル名(Test~1.jpgなど)に変換して回避
 - ファイルの直接ドロップ、ファイルを開く、フォルダに開くに対応
