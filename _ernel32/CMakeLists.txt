cmake_minimum_required(VERSION 3.8)
project (_ernel32)

add_definitions(-D_UNICODE -DUNICODE)

add_library(_ernel32 SHARED _ernel32.cpp _ernel32.def)

set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} /OPT:REF /MANIFEST:NO")
