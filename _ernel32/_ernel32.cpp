// _ernel32.cpp : _ernel32のエントリポイント
//

#include <windows.h>

#include <atlbase.h>
#include <atlconv.h>
#include <atlsimpcoll.h>

FARPROC p_CloseHandle;
FARPROC p_CompareFileTime;
FARPROC p_CompareStringA;
FARPROC p_CopyFileA;
FARPROC p_CreateDirectoryA;
FARPROC p_CreateFileA;
FARPROC p_CreateMutexA;
FARPROC p_CreateThread;
FARPROC p_DeleteCriticalSection;
FARPROC p_DeleteFileA;
FARPROC p_DuplicateHandle;
FARPROC p_EnterCriticalSection;
FARPROC p_ExitProcess;
FARPROC p_ExitThread;
FARPROC p_FileTimeToLocalFileTime;
FARPROC p_FileTimeToSystemTime;

//FARPROC p_FindClose;
typedef BOOL (WINAPI *D_FindClose)(HANDLE);
D_FindClose p_FindClose;

FARPROC p_FindCloseChangeNotification;
FARPROC p_FindFirstChangeNotificationA;

//FARPROC p_FindFirstFileA;
typedef HANDLE (WINAPI *D_FindFirstFileA)(LPCSTR, LPWIN32_FIND_DATAA);
D_FindFirstFileA p_FindFirstFileA;

//FARPROC p_FindFirstFileW;
//typedef HANDLE (WINAPI *D_FindFirstFileW)(LPCSTR, LPWIN32_FIND_DATAW);
//D_FindFirstFileW p_FindFirstFileW;

FARPROC p_FindNextChangeNotification;

//FARPROC p_FindNextFileA;
typedef BOOL (WINAPI *D_FindNextFileA)(HANDLE, LPWIN32_FIND_DATAA);
D_FindNextFileA p_FindNextFileA;

//FARPROC p_FindNextFileW;
//typedef BOOL (WINAPI *D_FindNextFileW)(HANDLE, LPWIN32_FIND_DATAW);
//D_FindNextFileW p_FindNextFileW;

FARPROC p_FlushFileBuffers;
FARPROC p_FormatMessageA;
FARPROC p_FreeEnvironmentStringsA;
FARPROC p_FreeEnvironmentStringsW;
FARPROC p_FreeLibrary;
FARPROC p_GetACP;
FARPROC p_GetCPInfo;
FARPROC p_GetCommandLineA;
FARPROC p_GetCurrentProcess;
FARPROC p_GetCurrentProcessId;
FARPROC p_GetCurrentThread;
FARPROC p_GetCurrentThreadId;
FARPROC p_GetEnvironmentStrings;
FARPROC p_GetEnvironmentStringsW;
FARPROC p_GetFileAttributesA;
FARPROC p_GetFileSize;
FARPROC p_GetFileType;
FARPROC p_GetFullPathNameA;
FARPROC p_GetLastError;
FARPROC p_GetLocalTime;
FARPROC p_GetLocaleInfoA;
FARPROC p_GetModuleFileNameA;
FARPROC p_GetModuleHandleA;
FARPROC p_GetOEMCP;
FARPROC p_GetProcAddress;
FARPROC p_GetStartupInfoA;
FARPROC p_GetStdHandle;
FARPROC p_GetStringTypeA;
FARPROC p_GetStringTypeW;
FARPROC p_GetSystemInfo;
FARPROC p_GetSystemTimeAsFileTime;
FARPROC p_GetTempPathA;
FARPROC p_GetThreadPriority;
FARPROC p_GetTickCount;
FARPROC p_GetVersionExA;
FARPROC p_GetWindowsDirectoryA;
FARPROC p_GlobalAlloc;
FARPROC p_GlobalFree;
FARPROC p_GlobalLock;
FARPROC p_GlobalMemoryStatus;
FARPROC p_GlobalSize;
FARPROC p_GlobalUnlock;
FARPROC p_HeapAlloc;
FARPROC p_HeapCreate;
FARPROC p_HeapDestroy;
FARPROC p_HeapFree;
FARPROC p_HeapReAlloc;
FARPROC p_HeapSize;
FARPROC p_InitializeCriticalSection;
FARPROC p_InterlockedDecrement;
FARPROC p_InterlockedIncrement;
FARPROC p_IsBadCodePtr;
FARPROC p_IsBadReadPtr;
FARPROC p_IsBadWritePtr;
FARPROC p_IsDBCSLeadByte;
FARPROC p_LCMapStringA;
FARPROC p_LCMapStringW;
FARPROC p_LeaveCriticalSection;
FARPROC p_LoadLibraryA;
FARPROC p_LocalAlloc;
FARPROC p_LocalFree;
FARPROC p_LocalLock;
FARPROC p_LocalReAlloc;
FARPROC p_LocalSize;
FARPROC p_LocalUnlock;
FARPROC p_MoveFileA;
FARPROC p_MulDiv;
FARPROC p_MultiByteToWideChar;
FARPROC p_OutputDebugStringA;
FARPROC p_QueryPerformanceCounter;
FARPROC p_RaiseException;
FARPROC p_ReadFile;
FARPROC p_RemoveDirectoryA;
FARPROC p_ResumeThread;
FARPROC p_RtlUnwind;
FARPROC p_SetCurrentDirectoryA;
FARPROC p_SetEndOfFile;
FARPROC p_SetErrorMode;
FARPROC p_SetFilePointer;
FARPROC p_SetHandleCount;
FARPROC p_SetLastError;
FARPROC p_SetStdHandle;
FARPROC p_SetThreadPriority;
FARPROC p_SetUnhandledExceptionFilter;
FARPROC p_Sleep;
FARPROC p_SuspendThread;
FARPROC p_SystemTimeToFileTime;
FARPROC p_TerminateProcess;
FARPROC p_TlsAlloc;
FARPROC p_TlsFree;
FARPROC p_TlsGetValue;
FARPROC p_TlsSetValue;
FARPROC p_UnhandledExceptionFilter;
FARPROC p_VirtualAlloc;
FARPROC p_VirtualFree;
FARPROC p_VirtualProtect;
FARPROC p_VirtualQuery;
FARPROC p_WaitForMultipleObjects;
FARPROC p_WaitForSingleObject;
FARPROC p_WideCharToMultiByte;
FARPROC p_WriteFile;
FARPROC p_lstrcatA;
FARPROC p_lstrcmpA;
FARPROC p_lstrcmpiA;

FARPROC p_lstrcpyA;
//typedef LPTSTR (WINAPI *D_lstrcpyA)(LPTSTR, LPTSTR);
//D_lstrcpyA p_lstrcpyA;

FARPROC p_lstrcpynA;
FARPROC p_lstrlenA;

HINSTANCE h_original;

CSimpleMap<HANDLE, wchar_t *> dirmap;

BOOL APIENTRY DllMain( HANDLE hModule, 
                    DWORD  ul_reason_for_call, 
                    LPVOID lpReserved
                  )
{
 switch( ul_reason_for_call )
 {
 case DLL_PROCESS_ATTACH:
     h_original = LoadLibraryA( "kernel32.dll" );
     if ( h_original == NULL )
         return FALSE;
     
       p_CloseHandle = GetProcAddress( h_original, "CloseHandle" );
       p_CompareFileTime = GetProcAddress( h_original, "CompareFileTime" );
       p_CompareStringA = GetProcAddress( h_original, "CompareStringA" );
       p_CopyFileA = GetProcAddress( h_original, "CopyFileA" );
       p_CreateDirectoryA = GetProcAddress( h_original, "CreateDirectoryA" );
       p_CreateFileA = GetProcAddress( h_original, "CreateFileA" );
       p_CreateMutexA = GetProcAddress( h_original, "CreateMutexA" );
       p_CreateThread = GetProcAddress( h_original, "CreateThread" );
       p_DeleteCriticalSection = GetProcAddress( h_original, "DeleteCriticalSection" );
       p_DeleteFileA = GetProcAddress( h_original, "DeleteFileA" );
       p_DuplicateHandle = GetProcAddress( h_original, "DuplicateHandle" );
       p_EnterCriticalSection = GetProcAddress( h_original, "EnterCriticalSection" );
       p_ExitProcess = GetProcAddress( h_original, "ExitProcess" );
       p_ExitThread = GetProcAddress( h_original, "ExitThread" );
       p_FileTimeToLocalFileTime = GetProcAddress( h_original, "FileTimeToLocalFileTime" );
       p_FileTimeToSystemTime = GetProcAddress( h_original, "FileTimeToSystemTime" );
       p_FindClose = (D_FindClose)GetProcAddress( h_original, "FindClose" );
       p_FindCloseChangeNotification = GetProcAddress( h_original, "FindCloseChangeNotification" );
       p_FindFirstChangeNotificationA = GetProcAddress( h_original, "FindFirstChangeNotificationA" );
       p_FindFirstFileA = (D_FindFirstFileA)GetProcAddress( h_original, "FindFirstFileA" );
       //p_FindFirstFileW = (D_FindFirstFileW)GetProcAddress( h_original, "FindFirstFileW" );
       p_FindNextChangeNotification = GetProcAddress( h_original, "FindNextChangeNotification" );
       p_FindNextFileA = (D_FindNextFileA)GetProcAddress( h_original, "FindNextFileA" );
       //p_FindNextFileW = (D_FindNextFileW)GetProcAddress( h_original, "FindNextFileW" );
       p_FlushFileBuffers = GetProcAddress( h_original, "FlushFileBuffers" );
       p_FormatMessageA = GetProcAddress( h_original, "FormatMessageA" );
       p_FreeEnvironmentStringsA = GetProcAddress( h_original, "FreeEnvironmentStringsA" );
       p_FreeEnvironmentStringsW = GetProcAddress( h_original, "FreeEnvironmentStringsW" );
       p_FreeLibrary = GetProcAddress( h_original, "FreeLibrary" );
       p_GetACP = GetProcAddress( h_original, "GetACP" );
       p_GetCPInfo = GetProcAddress( h_original, "GetCPInfo" );
       p_GetCommandLineA = GetProcAddress( h_original, "GetCommandLineA" );
       p_GetCurrentProcess = GetProcAddress( h_original, "GetCurrentProcess" );
       p_GetCurrentProcessId = GetProcAddress( h_original, "GetCurrentProcessId" );
       p_GetCurrentThread = GetProcAddress( h_original, "GetCurrentThread" );
       p_GetCurrentThreadId = GetProcAddress( h_original, "GetCurrentThreadId" );
       p_GetEnvironmentStrings = GetProcAddress( h_original, "GetEnvironmentStrings" );
       p_GetEnvironmentStringsW = GetProcAddress( h_original, "GetEnvironmentStringsW" );
       p_GetFileAttributesA = GetProcAddress( h_original, "GetFileAttributesA" );
       p_GetFileSize = GetProcAddress( h_original, "GetFileSize" );
       p_GetFileType = GetProcAddress( h_original, "GetFileType" );
       p_GetFullPathNameA = GetProcAddress( h_original, "GetFullPathNameA" );
       p_GetLastError = GetProcAddress( h_original, "GetLastError" );
       p_GetLocalTime = GetProcAddress( h_original, "GetLocalTime" );
       p_GetLocaleInfoA = GetProcAddress( h_original, "GetLocaleInfoA" );
       p_GetModuleFileNameA = GetProcAddress( h_original, "GetModuleFileNameA" );
       p_GetModuleHandleA = GetProcAddress( h_original, "GetModuleHandleA" );
       p_GetOEMCP = GetProcAddress( h_original, "GetOEMCP" );
       p_GetProcAddress = GetProcAddress( h_original, "GetProcAddress" );
       
       //p_GetShortPathNameW = (D_GetShortPathNameW)GetProcAddress( h_original, "GetShortPathNameW" );
       
       p_GetStartupInfoA = GetProcAddress( h_original, "GetStartupInfoA" );
       p_GetStdHandle = GetProcAddress( h_original, "GetStdHandle" );
       p_GetStringTypeA = GetProcAddress( h_original, "GetStringTypeA" );
       p_GetStringTypeW = GetProcAddress( h_original, "GetStringTypeW" );
       p_GetSystemInfo = GetProcAddress( h_original, "GetSystemInfo" );
       p_GetSystemTimeAsFileTime = GetProcAddress( h_original, "GetSystemTimeAsFileTime" );
       p_GetTempPathA = GetProcAddress( h_original, "GetTempPathA" );
       p_GetThreadPriority = GetProcAddress( h_original, "GetThreadPriority" );
       p_GetTickCount = GetProcAddress( h_original, "GetTickCount" );
       p_GetVersionExA = GetProcAddress( h_original, "GetVersionExA" );
       p_GetWindowsDirectoryA = GetProcAddress( h_original, "GetWindowsDirectoryA" );
       p_GlobalAlloc = GetProcAddress( h_original, "GlobalAlloc" );
       p_GlobalFree = GetProcAddress( h_original, "GlobalFree" );
       p_GlobalLock = GetProcAddress( h_original, "GlobalLock" );
       p_GlobalMemoryStatus = GetProcAddress( h_original, "GlobalMemoryStatus" );
       p_GlobalSize = GetProcAddress( h_original, "GlobalSize" );
       p_GlobalUnlock = GetProcAddress( h_original, "GlobalUnlock" );
       p_HeapAlloc = GetProcAddress( h_original, "HeapAlloc" );
       p_HeapCreate = GetProcAddress( h_original, "HeapCreate" );
       p_HeapDestroy = GetProcAddress( h_original, "HeapDestroy" );
       p_HeapFree = GetProcAddress( h_original, "HeapFree" );
       p_HeapReAlloc = GetProcAddress( h_original, "HeapReAlloc" );
       p_HeapSize = GetProcAddress( h_original, "HeapSize" );
       p_InitializeCriticalSection = GetProcAddress( h_original, "InitializeCriticalSection" );
       p_InterlockedDecrement = GetProcAddress( h_original, "InterlockedDecrement" );
       p_InterlockedIncrement = GetProcAddress( h_original, "InterlockedIncrement" );
       p_IsBadCodePtr = GetProcAddress( h_original, "IsBadCodePtr" );
       p_IsBadReadPtr = GetProcAddress( h_original, "IsBadReadPtr" );
       p_IsBadWritePtr = GetProcAddress( h_original, "IsBadWritePtr" );
       p_IsDBCSLeadByte = GetProcAddress( h_original, "IsDBCSLeadByte" );
       p_LCMapStringA = GetProcAddress( h_original, "LCMapStringA" );
       p_LCMapStringW = GetProcAddress( h_original, "LCMapStringW" );
       p_LeaveCriticalSection = GetProcAddress( h_original, "LeaveCriticalSection" );
       p_LoadLibraryA = GetProcAddress( h_original, "LoadLibraryA" );
       p_LocalAlloc = GetProcAddress( h_original, "LocalAlloc" );
       p_LocalFree = GetProcAddress( h_original, "LocalFree" );
       p_LocalLock = GetProcAddress( h_original, "LocalLock" );
       p_LocalReAlloc = GetProcAddress( h_original, "LocalReAlloc" );
       p_LocalSize = GetProcAddress( h_original, "LocalSize" );
       p_LocalUnlock = GetProcAddress( h_original, "LocalUnlock" );
       p_MoveFileA = GetProcAddress( h_original, "MoveFileA" );
       p_MulDiv = GetProcAddress( h_original, "MulDiv" );
       p_MultiByteToWideChar = GetProcAddress( h_original, "MultiByteToWideChar" );
       p_OutputDebugStringA = GetProcAddress( h_original, "OutputDebugStringA" );
       p_QueryPerformanceCounter = GetProcAddress( h_original, "QueryPerformanceCounter" );
       p_RaiseException = GetProcAddress( h_original, "RaiseException" );
       p_ReadFile = GetProcAddress( h_original, "ReadFile" );
       p_RemoveDirectoryA = GetProcAddress( h_original, "RemoveDirectoryA" );
       p_ResumeThread = GetProcAddress( h_original, "ResumeThread" );
       p_RtlUnwind = GetProcAddress( h_original, "RtlUnwind" );
       p_SetCurrentDirectoryA = GetProcAddress( h_original, "SetCurrentDirectoryA" );
       p_SetEndOfFile = GetProcAddress( h_original, "SetEndOfFile" );
       p_SetErrorMode = GetProcAddress( h_original, "SetErrorMode" );
       p_SetFilePointer = GetProcAddress( h_original, "SetFilePointer" );
       p_SetHandleCount = GetProcAddress( h_original, "SetHandleCount" );
       p_SetLastError = GetProcAddress( h_original, "SetLastError" );
       p_SetStdHandle = GetProcAddress( h_original, "SetStdHandle" );
       p_SetThreadPriority = GetProcAddress( h_original, "SetThreadPriority" );
       p_SetUnhandledExceptionFilter = GetProcAddress( h_original, "SetUnhandledExceptionFilter" );
       p_Sleep = GetProcAddress( h_original, "Sleep" );
       p_SuspendThread = GetProcAddress( h_original, "SuspendThread" );
       p_SystemTimeToFileTime = GetProcAddress( h_original, "SystemTimeToFileTime" );
       p_TerminateProcess = GetProcAddress( h_original, "TerminateProcess" );
       p_TlsAlloc = GetProcAddress( h_original, "TlsAlloc" );
       p_TlsFree = GetProcAddress( h_original, "TlsFree" );
       p_TlsGetValue = GetProcAddress( h_original, "TlsGetValue" );
       p_TlsSetValue = GetProcAddress( h_original, "TlsSetValue" );
       p_UnhandledExceptionFilter = GetProcAddress( h_original, "UnhandledExceptionFilter" );
       p_VirtualAlloc = GetProcAddress( h_original, "VirtualAlloc" );
       p_VirtualFree = GetProcAddress( h_original, "VirtualFree" );
       p_VirtualProtect = GetProcAddress( h_original, "VirtualProtect" );
       p_VirtualQuery = GetProcAddress( h_original, "VirtualQuery" );
       p_WaitForMultipleObjects = GetProcAddress( h_original, "WaitForMultipleObjects" );
       p_WaitForSingleObject = GetProcAddress( h_original, "WaitForSingleObject" );
       p_WideCharToMultiByte = GetProcAddress( h_original, "WideCharToMultiByte" );
       p_WriteFile = GetProcAddress( h_original, "WriteFile" );
       p_lstrcatA = GetProcAddress( h_original, "lstrcatA" );
       p_lstrcmpA = GetProcAddress( h_original, "lstrcmpA" );
       p_lstrcmpiA = GetProcAddress( h_original, "lstrcmpiA" );
       p_lstrcpyA = /*(D_lstrcpyA)*/GetProcAddress( h_original, "lstrcpyA" );
       p_lstrcpynA = GetProcAddress( h_original, "lstrcpynA" );
       p_lstrlenA = GetProcAddress( h_original, "lstrlenA" );
       
     break;
 case DLL_THREAD_ATTACH:
     break;
 case DLL_THREAD_DETACH:
     break;
 case DLL_PROCESS_DETACH:
     FreeLibrary( h_original );
     
     {
       int nDirmap = dirmap.GetSize();
       for(int i=0;i<nDirmap;i++)
       {
         wchar_t *d = dirmap.GetValueAt(i);
         if(d!=NULL)
         {
           free(d);
         }
       }
       dirmap.RemoveAll();
    }
     break;
 default:
     break;
 }
 return TRUE;
 }
















// _ernel32_dummy.cpp : デフォルトの処理を行うエクスポート関数
//
 
__declspec( naked ) void d_CloseHandle() { _asm{ jmp p_CloseHandle } }
__declspec( naked ) void d_CompareFileTime() { _asm{ jmp p_CompareFileTime } }
__declspec( naked ) void d_CompareStringA() { _asm{ jmp p_CompareStringA } }
__declspec( naked ) void d_CopyFileA() { _asm{ jmp p_CopyFileA } }
__declspec( naked ) void d_CreateDirectoryA() { _asm{ jmp p_CreateDirectoryA } }
__declspec( naked ) void d_CreateFileA() { _asm{ jmp p_CreateFileA } }
__declspec( naked ) void d_CreateMutexA() { _asm{ jmp p_CreateMutexA } }
__declspec( naked ) void d_CreateThread() { _asm{ jmp p_CreateThread } }
__declspec( naked ) void d_DeleteCriticalSection() { _asm{ jmp p_DeleteCriticalSection } }
__declspec( naked ) void d_DeleteFileA() { _asm{ jmp p_DeleteFileA } }
__declspec( naked ) void d_DuplicateHandle() { _asm{ jmp p_DuplicateHandle } }
__declspec( naked ) void d_EnterCriticalSection() { _asm{ jmp p_EnterCriticalSection } }
__declspec( naked ) void d_ExitProcess() { _asm{ jmp p_ExitProcess } }
__declspec( naked ) void d_ExitThread() { _asm{ jmp p_ExitThread } }
__declspec( naked ) void d_FileTimeToLocalFileTime() { _asm{ jmp p_FileTimeToLocalFileTime } }
__declspec( naked ) void d_FileTimeToSystemTime() { _asm{ jmp p_FileTimeToSystemTime } }

//__declspec( naked ) void d_FindClose() { _asm{ jmp p_FindClose } }
BOOL WINAPI d_FindClose(HANDLE hFindHandle)
{
  int f = dirmap.FindKey(hFindHandle);
  if(f!=-1)
  {
    wchar_t *b = dirmap.GetValueAt(f);
    if(b!=NULL)free(b);
    dirmap.RemoveAt(f);
  }
  
  return FindClose(hFindHandle);
}

__declspec( naked ) void d_FindCloseChangeNotification() { _asm{ jmp p_FindCloseChangeNotification } }
__declspec( naked ) void d_FindFirstChangeNotificationA() { _asm{ jmp p_FindFirstChangeNotificationA } }


void Convert2ShortFileName(LPWIN32_FIND_DATAW src, LPWIN32_FIND_DATAA dst, HANDLE hFind)
{
  dst->dwFileAttributes = src->dwFileAttributes;
  dst->ftCreationTime   = src->ftCreationTime;
  dst->ftLastAccessTime = src->ftLastAccessTime;
  dst->ftLastWriteTime  = src->ftLastWriteTime;
  dst->nFileSizeHigh    = src->nFileSizeHigh;
  dst->nFileSizeLow     = src->nFileSizeLow;
  dst->dwReserved0      = src->dwReserved0;
  dst->dwReserved1      = src->dwReserved1;
  
  /*
  OutputDebugStringA("Convert2ShortFileName: ");
  OutputDebugStringA("  src->cFileName: ");
  OutputDebugStringW(src->cFileName);
  OutputDebugStringA("  src->cAlternateFileName: ");
  OutputDebugStringW(src->cAlternateFileName);
  */
  
  
  if((src->cAlternateFileName)[0]==NULL)
  {
    wchar_t *tmppath = (wchar_t *)malloc(sizeof(wchar_t)*_MAX_PATH*2+1);
    int f = dirmap.FindKey(hFind);
    if(f!=-1)
    {
      wchar_t *finddir = dirmap.GetValueAt(f);
      lstrcpynW(tmppath, finddir, _MAX_PATH*2);
    }
    lstrcatW(tmppath, src->cFileName);
    DWORD dwShortPath = GetShortPathNameW(tmppath, NULL, 0);
    wchar_t *shortpath = (wchar_t *)malloc(sizeof(wchar_t)*(dwShortPath+2));
    GetShortPathNameW(tmppath, shortpath, dwShortPath+1);
    PathStripPathW(shortpath);
    lstrcpynW(src->cAlternateFileName, shortpath, 14);
    free(shortpath);
    free(tmppath);
    //OutputDebugStringA("  src->cAlternateFileName: ");
    //OutputDebugStringW(src->cAlternateFileName);
  }
  lstrcpyA(dst->cAlternateFileName, CW2A(src->cAlternateFileName));
  lstrcpyA(dst->cFileName, dst->cAlternateFileName);
  //OutputDebugStringA("  dst->cAlternateFileName: ");
  //OutputDebugStringA(dst->cAlternateFileName);
}

void SetFindDir(HANDLE hFind, LPCWSTR lpFileName)
{
  wchar_t *tmppath = (wchar_t *)malloc(sizeof(wchar_t)*_MAX_PATH*2+2);
  lstrcpynW(tmppath, lpFileName, _MAX_PATH*2+1);
  PathRemoveFileSpecW(tmppath);
  lstrcatW(tmppath, L"\\");
  
  wchar_t *finddir = (wchar_t *)malloc(sizeof(wchar_t)*_MAX_PATH*2+1);
  lstrcpyW(finddir, tmppath);
  
  int f = dirmap.FindKey(hFind);
  if(f=!-1)
  {
    wchar_t *b = dirmap.GetValueAt(f);
    if(b!=NULL)free(b);
    dirmap.SetAtIndex(f, hFind, finddir);
  } else {
    dirmap.Add(hFind, finddir);
  }
  
  free(tmppath);
}

//__declspec( naked ) void d_FindFirstFileA() { _asm{ jmp p_FindFirstFileA } }
HANDLE WINAPI d_FindFirstFileA(LPCSTR lpFileName, LPWIN32_FIND_DATAA lpFindFileData)
{
  //OutputDebugStringA("d_FindFirstFileA: ");
  //OutputDebugStringA(lpFileName);
  WIN32_FIND_DATAW fd = {0};
  HANDLE ret = FindFirstFileExW(CA2W(lpFileName), FindExInfoBasic, &fd, FindExSearchNameMatch, NULL, 0);
  DWORD err = GetLastError();
  /*char t[50];
  sprintf(t, "0x%08X", err);
  OutputDebugStringA(t);
  */
  if(ret!=INVALID_HANDLE_VALUE)
  {
    SetFindDir(ret, CA2W(lpFileName));
    Convert2ShortFileName(&fd, lpFindFileData, ret);
  }
  return ret;
}


__declspec( naked ) void d_FindNextChangeNotification() { _asm{ jmp p_FindNextChangeNotification } }

//__declspec( naked ) void d_FindNextFileA() { _asm{ jmp p_FindNextFileA } }
BOOL WINAPI d_FindNextFileA(HANDLE hFindFile, LPWIN32_FIND_DATAA lpFindFileData)
{
  //OutputDebugStringA("d_FindNextFileA\n");
  WIN32_FIND_DATAW fd = {0};
  BOOL ret = FindNextFileW(hFindFile, &fd);
  if(ret==TRUE)Convert2ShortFileName(&fd, lpFindFileData, hFindFile);
  return ret;
}

__declspec( naked ) void d_FlushFileBuffers() { _asm{ jmp p_FlushFileBuffers } }
__declspec( naked ) void d_FormatMessageA() { _asm{ jmp p_FormatMessageA } }
__declspec( naked ) void d_FreeEnvironmentStringsA() { _asm{ jmp p_FreeEnvironmentStringsA } }
__declspec( naked ) void d_FreeEnvironmentStringsW() { _asm{ jmp p_FreeEnvironmentStringsW } }
__declspec( naked ) void d_FreeLibrary() { _asm{ jmp p_FreeLibrary } }
__declspec( naked ) void d_GetACP() { _asm{ jmp p_GetACP } }
__declspec( naked ) void d_GetCPInfo() { _asm{ jmp p_GetCPInfo } }
__declspec( naked ) void d_GetCommandLineA() { _asm{ jmp p_GetCommandLineA } }
__declspec( naked ) void d_GetCurrentProcess() { _asm{ jmp p_GetCurrentProcess } }
__declspec( naked ) void d_GetCurrentProcessId() { _asm{ jmp p_GetCurrentProcessId } }
__declspec( naked ) void d_GetCurrentThread() { _asm{ jmp p_GetCurrentThread } }
__declspec( naked ) void d_GetCurrentThreadId() { _asm{ jmp p_GetCurrentThreadId } }
__declspec( naked ) void d_GetEnvironmentStrings() { _asm{ jmp p_GetEnvironmentStrings } }
__declspec( naked ) void d_GetEnvironmentStringsW() { _asm{ jmp p_GetEnvironmentStringsW } }
__declspec( naked ) void d_GetFileAttributesA() { _asm{ jmp p_GetFileAttributesA } }
__declspec( naked ) void d_GetFileSize() { _asm{ jmp p_GetFileSize } }
__declspec( naked ) void d_GetFileType() { _asm{ jmp p_GetFileType } }
__declspec( naked ) void d_GetFullPathNameA() { _asm{ jmp p_GetFullPathNameA } }
__declspec( naked ) void d_GetLastError() { _asm{ jmp p_GetLastError } }
__declspec( naked ) void d_GetLocalTime() { _asm{ jmp p_GetLocalTime } }
__declspec( naked ) void d_GetLocaleInfoA() { _asm{ jmp p_GetLocaleInfoA } }
__declspec( naked ) void d_GetModuleFileNameA() { _asm{ jmp p_GetModuleFileNameA } }
__declspec( naked ) void d_GetModuleHandleA() { _asm{ jmp p_GetModuleHandleA } }
__declspec( naked ) void d_GetOEMCP() { _asm{ jmp p_GetOEMCP } }
__declspec( naked ) void d_GetProcAddress() { _asm{ jmp p_GetProcAddress } }
__declspec( naked ) void d_GetStartupInfoA() { _asm{ jmp p_GetStartupInfoA } }
__declspec( naked ) void d_GetStdHandle() { _asm{ jmp p_GetStdHandle } }
__declspec( naked ) void d_GetStringTypeA() { _asm{ jmp p_GetStringTypeA } }
__declspec( naked ) void d_GetStringTypeW() { _asm{ jmp p_GetStringTypeW } }
__declspec( naked ) void d_GetSystemInfo() { _asm{ jmp p_GetSystemInfo } }
__declspec( naked ) void d_GetSystemTimeAsFileTime() { _asm{ jmp p_GetSystemTimeAsFileTime } }
__declspec( naked ) void d_GetTempPathA() { _asm{ jmp p_GetTempPathA } }
__declspec( naked ) void d_GetThreadPriority() { _asm{ jmp p_GetThreadPriority } }
__declspec( naked ) void d_GetTickCount() { _asm{ jmp p_GetTickCount } }
__declspec( naked ) void d_GetVersionExA() { _asm{ jmp p_GetVersionExA } }
__declspec( naked ) void d_GetWindowsDirectoryA() { _asm{ jmp p_GetWindowsDirectoryA } }
__declspec( naked ) void d_GlobalAlloc() { _asm{ jmp p_GlobalAlloc } }
__declspec( naked ) void d_GlobalFree() { _asm{ jmp p_GlobalFree } }
__declspec( naked ) void d_GlobalLock() { _asm{ jmp p_GlobalLock } }
__declspec( naked ) void d_GlobalMemoryStatus() { _asm{ jmp p_GlobalMemoryStatus } }
__declspec( naked ) void d_GlobalSize() { _asm{ jmp p_GlobalSize } }
__declspec( naked ) void d_GlobalUnlock() { _asm{ jmp p_GlobalUnlock } }
__declspec( naked ) void d_HeapAlloc() { _asm{ jmp p_HeapAlloc } }
__declspec( naked ) void d_HeapCreate() { _asm{ jmp p_HeapCreate } }
__declspec( naked ) void d_HeapDestroy() { _asm{ jmp p_HeapDestroy } }
__declspec( naked ) void d_HeapFree() { _asm{ jmp p_HeapFree } }
__declspec( naked ) void d_HeapReAlloc() { _asm{ jmp p_HeapReAlloc } }
__declspec( naked ) void d_HeapSize() { _asm{ jmp p_HeapSize } }
__declspec( naked ) void d_InitializeCriticalSection() { _asm{ jmp p_InitializeCriticalSection } }
__declspec( naked ) void d_InterlockedDecrement() { _asm{ jmp p_InterlockedDecrement } }
__declspec( naked ) void d_InterlockedIncrement() { _asm{ jmp p_InterlockedIncrement } }
__declspec( naked ) void d_IsBadCodePtr() { _asm{ jmp p_IsBadCodePtr } }
__declspec( naked ) void d_IsBadReadPtr() { _asm{ jmp p_IsBadReadPtr } }
__declspec( naked ) void d_IsBadWritePtr() { _asm{ jmp p_IsBadWritePtr } }
__declspec( naked ) void d_IsDBCSLeadByte() { _asm{ jmp p_IsDBCSLeadByte } }
__declspec( naked ) void d_LCMapStringA() { _asm{ jmp p_LCMapStringA } }
__declspec( naked ) void d_LCMapStringW() { _asm{ jmp p_LCMapStringW } }
__declspec( naked ) void d_LeaveCriticalSection() { _asm{ jmp p_LeaveCriticalSection } }
__declspec( naked ) void d_LoadLibraryA() { _asm{ jmp p_LoadLibraryA } }
__declspec( naked ) void d_LocalAlloc() { _asm{ jmp p_LocalAlloc } }
__declspec( naked ) void d_LocalFree() { _asm{ jmp p_LocalFree } }
__declspec( naked ) void d_LocalLock() { _asm{ jmp p_LocalLock } }
__declspec( naked ) void d_LocalReAlloc() { _asm{ jmp p_LocalReAlloc } }
__declspec( naked ) void d_LocalSize() { _asm{ jmp p_LocalSize } }
__declspec( naked ) void d_LocalUnlock() { _asm{ jmp p_LocalUnlock } }
__declspec( naked ) void d_MoveFileA() { _asm{ jmp p_MoveFileA } }
__declspec( naked ) void d_MulDiv() { _asm{ jmp p_MulDiv } }
__declspec( naked ) void d_MultiByteToWideChar() { _asm{ jmp p_MultiByteToWideChar } }
__declspec( naked ) void d_OutputDebugStringA() { _asm{ jmp p_OutputDebugStringA } }
__declspec( naked ) void d_QueryPerformanceCounter() { _asm{ jmp p_QueryPerformanceCounter } }
__declspec( naked ) void d_RaiseException() { _asm{ jmp p_RaiseException } }
__declspec( naked ) void d_ReadFile() { _asm{ jmp p_ReadFile } }
__declspec( naked ) void d_RemoveDirectoryA() { _asm{ jmp p_RemoveDirectoryA } }
__declspec( naked ) void d_ResumeThread() { _asm{ jmp p_ResumeThread } }
__declspec( naked ) void d_RtlUnwind() { _asm{ jmp p_RtlUnwind } }
__declspec( naked ) void d_SetCurrentDirectoryA() { _asm{ jmp p_SetCurrentDirectoryA } }
__declspec( naked ) void d_SetEndOfFile() { _asm{ jmp p_SetEndOfFile } }
__declspec( naked ) void d_SetErrorMode() { _asm{ jmp p_SetErrorMode } }
__declspec( naked ) void d_SetFilePointer() { _asm{ jmp p_SetFilePointer } }
__declspec( naked ) void d_SetHandleCount() { _asm{ jmp p_SetHandleCount } }
__declspec( naked ) void d_SetLastError() { _asm{ jmp p_SetLastError } }
__declspec( naked ) void d_SetStdHandle() { _asm{ jmp p_SetStdHandle } }
__declspec( naked ) void d_SetThreadPriority() { _asm{ jmp p_SetThreadPriority } }
__declspec( naked ) void d_SetUnhandledExceptionFilter() { _asm{ jmp p_SetUnhandledExceptionFilter } }
__declspec( naked ) void d_Sleep() { _asm{ jmp p_Sleep } }
__declspec( naked ) void d_SuspendThread() { _asm{ jmp p_SuspendThread } }
__declspec( naked ) void d_SystemTimeToFileTime() { _asm{ jmp p_SystemTimeToFileTime } }
__declspec( naked ) void d_TerminateProcess() { _asm{ jmp p_TerminateProcess } }
__declspec( naked ) void d_TlsAlloc() { _asm{ jmp p_TlsAlloc } }
__declspec( naked ) void d_TlsFree() { _asm{ jmp p_TlsFree } }
__declspec( naked ) void d_TlsGetValue() { _asm{ jmp p_TlsGetValue } }
__declspec( naked ) void d_TlsSetValue() { _asm{ jmp p_TlsSetValue } }
__declspec( naked ) void d_UnhandledExceptionFilter() { _asm{ jmp p_UnhandledExceptionFilter } }
__declspec( naked ) void d_VirtualAlloc() { _asm{ jmp p_VirtualAlloc } }
__declspec( naked ) void d_VirtualFree() { _asm{ jmp p_VirtualFree } }
__declspec( naked ) void d_VirtualProtect() { _asm{ jmp p_VirtualProtect } }
__declspec( naked ) void d_VirtualQuery() { _asm{ jmp p_VirtualQuery } }
__declspec( naked ) void d_WaitForMultipleObjects() { _asm{ jmp p_WaitForMultipleObjects } }
__declspec( naked ) void d_WaitForSingleObject() { _asm{ jmp p_WaitForSingleObject } }
__declspec( naked ) void d_WideCharToMultiByte() { _asm{ jmp p_WideCharToMultiByte } }
__declspec( naked ) void d_WriteFile() { _asm{ jmp p_WriteFile } }
__declspec( naked ) void d_lstrcatA() { _asm{ jmp p_lstrcatA } }
__declspec( naked ) void d_lstrcmpA() { _asm{ jmp p_lstrcmpA } }
__declspec( naked ) void d_lstrcmpiA() { _asm{ jmp p_lstrcmpiA } }
__declspec( naked ) void d_lstrcpyA() { _asm{ jmp p_lstrcpyA } }
__declspec( naked ) void d_lstrcpynA() { _asm{ jmp p_lstrcpynA } }
__declspec( naked ) void d_lstrlenA() { _asm{ jmp p_lstrlenA } }
